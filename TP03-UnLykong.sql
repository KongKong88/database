SHOW CREATE TABLE TABLE-NAME # standard for show type of Database (InnoDB)
#1. List all customer name and city
USE DATABASE producttest;
SELECT NAME, city 
FROM customer_t;

#2. List the information of customer from 'CA' (state)
SELECT * FROM customer_t WHERE state='CA';

#3. List ID, description and standard price of product made from "ash"
SELECT id, description, standard_price 
FROM product_t 
WHERE finish LIKE "%ash";

#4. List the information of product that is made from "ash" and cost between 15 and 80
SELECT *
FROM product_t 
WHERE finish LIKE "%ash" AND  standard_price BETWEEN 15 AND 80;

#5. show all product and sort result by standard price
SELECT * 
FROM product_t ORDER BY standard_price ASC;

#6. show the cross product between customer and order
SELECT * FROM customer_t JOIN order_t;

#7. Show the information of customer who has made order on “2004-04-01”. 
SELECT c.*
FROM customer_t AS c JOIN order_t AS o
	ON c.ID=o.customer_ID
WHERE order_date= "2004-04-01";

#8. Show information of products in the order number 106. 
SELECT p.*
FROM product_t AS p JOIN order_t AS o JOIN order_line_t AS ol
	ON p.ID=ol.product_ID AND ol.order_ID=o.ID 
WHERE o.ID=106;

#9. Show the information of customer who has made order number 106
SELECT c.* 
FROM customer_t AS c JOIN order_t AS o
	ON c.ID=o.customer_ID
WHERE o.ID=106;

#10. Show the order which contains book shelf. 
SELECT o.*
FROM product_t AS p JOIN order_t AS o JOIN order_line_t AS ol
	ON p.ID=ol.product_ID AND ol.order_ID=o.ID 
WHERE p.description LIKE "%book shelf%";

#11. Show the order which contains table lamp. 
SELECT o.* 
FROM product_t AS p JOIN order_t AS o JOIN order_line_t AS ol
	ON p.ID=ol.product_ID AND ol.order_ID=o.ID 
WHERE p.description LIKE "%table lamp%";

#12. Show the order which contain book shelf or table lamp. 
SELECT o.* 
FROM product_t AS p JOIN order_t AS o JOIN order_line_t AS ol
	ON p.ID=ol.product_ID AND ol.order_ID=o.ID 
WHERE p.description LIKE "%book shelf%" OR p.description LIKE "%table lamp%";

#13. Show the order which contain book shelf and table lamp
SELECT o.* 
FROM product_t AS p JOIN order_t AS o JOIN order_line_t AS ol
	ON p.ID=ol.product_ID AND ol.order_ID=o.ID
WHERE p.description LIKE "%book shelf%" AND o.ID IN
(SELECT ol.order_ID 
FROM order_line_t AS ol JOIN product_t AS p
	ON ol.product_ID=p.ID
WHERE p.description LIKE "%table lamp%");

#14. Show the order which contain book shelf but not table lamp. 
SELECT o.* 
FROM product_t AS p JOIN order_t AS o JOIN order_line_t AS ol
	ON p.ID=ol.product_ID AND ol.order_ID=o.ID
WHERE p.description LIKE "%book shelf%" AND o.ID NOT IN
(SELECT ol.order_ID 
FROM order_line_t AS ol JOIN product_t AS p
	ON ol.product_ID=p.ID
WHERE p.description LIKE "%table lamp%");
