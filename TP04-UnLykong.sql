#1. Show id and name of customer along with order id and order_date of the order they have
#made
SELECT c.ID, c.name, o.ID, o.order_date
FROM customer_t AS c JOIN order_t AS o
	ON c.ID=o.customer_ID;

#2. Show id and description of product and its quantity in each order
SELECT p.ID, p.description, ol.quantity
FROM product_t AS p JOIN order_line_t AS ol
	ON p.ID=ol.product_ID;  
	
#3. Show id and name of customer along with id and description of product they
#have ordered
SELECT c.ID, c.name, p.ID, p.description
FROM product_t AS p JOIN order_line_t AS ol JOIN customer_t AS c JOIN order_t AS o
	ON p.ID=ol.product_ID AND ol.order_ID=o.ID AND o.customer_ID=c.ID;
	
#4. Repeat from 1 and include the customers who has never made order to the result
#(by using outer join)
SELECT c.ID, c.name, o.ID, o.order_date
FROM customer_t AS c LEFT JOIN order_t AS o
	ON c.ID=o.customer_ID;
	
#5. Show product with the lowest standard price
SELECT * FROM product_t 
WHERE standard_price= (SELECT MIN(standard_price) FROM product_t);

#6. Show product with the same name
SELECT *
FROM product_t AS p1 JOIN  product_t AS p2
	ON p1.ID!=p2.ID AND p1.description=p2.description;
