#create database sailor
CREATE DATABASE sailor_s;
#use database sailor
USE DATABASE sailor_s;

#create table sailor
CREATE TABLE sailors(
	sid INT PRIMARY KEY,
	sname VARCHAR(20),
	rating INT,
	age INT
	);
	
#create table boat
CREATE TABLE boat(
	bid INT PRIMARY KEY,
	bname VARCHAR(20),
	color VARCHAR(20)
	);
	
#create table reserves
CREATE TABLE reserves(
	sid INT,
	bid INT,
	day DATE,
	CONSTRAINT PRIMARY KEY (sid,bid),
	CONSTRAINT FOREIGN KEY (sid) REFERENCES sailors(sid),
	CONSTRAINT FOREIGN KEY (bid) REFERENCES boat (bid)
);
#change the attribute age in saior into decimal 
ALTER TABLE sailors MODIFY age DECIMAL (3,1);

#check constraint to attribute rating in sailors, rating can have value from 0 to 10
ALTER TABLE sailors ADD CONSTRAINT CHECK (rating>=0 AND rating<=10);

#insert the following data to database
#insert data into sailor table
INSERT INTO sailors VALUES
(22,"Dustin",7,45),
(29,"Brustus",1,33),
(31,"Lubber",8,55.5),
(32,"Andy",8,25.5),
(58,"Rusty",10,35),
(64,"Horatio",7,35),
(71,"Zorba",10,16),
(74,"Horatio",9,35),
(85,"Art",3,25.5),
(95,"Bob",3,63.5);

#insert data into table reserve
INSERT INTO reserves VALUES 
(22,101,'1998-10-10'),
(22,102,'1998-10-10'),
(22,103,'1998-08-10'),
(22,104,'1998-07-10'),
(31,102,'1998-10-11'),
(31,103,'1998-06-11'),
(31,104,'1998-12-11'),
(64,101,'1998-05-09'),
(64,102,'1998-08-09'),
(74,103,'1998-08-09');

#insert data into table boat
INSERT INTO boat VALUES
(101,"Interlake","blue"),
(102,"Interlake","red"),
(103,"Clipper","green"),
(104,"Marine","red");