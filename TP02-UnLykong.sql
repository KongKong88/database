/*1. Insert the following row into the indicated table.
Then observe the response of DBMS

a. Duplicate entry '22' for primary key.
b. Column 'sid' cannot be null. Because it is a primary key
c. Incorrect for the column. Dara is a sname and datatype as a string
d. Duplicate entry 0 for primary key. 
sid have to be unique and different position of column
e. Cannot add or update a child row: a foreign key constraint fails
f. Can insert into table reserves because it isn't duplicate
*/

#2. Wrtie SQL query for each following questions
#a. List the name of sailors who are rating higher than 7
SELECT sname FROM sailors WHERE rating>7;

#b. List the name and rate of sailors who have rate higher than 7 and are younger than 50
SELECT sname FROM sailors WHERE rating>7 AND age<50;

#c. List information of the sailor named "Horatio"
SELECT * FROM sailors WHERE sname="Horatio";

#d. List information of sailors whose name start with "A" 
SELECT * FROM sailors WHERE sname LIKE "A%";

#e. List infromation of sailors whose name start with "A" and end with "Y"
SELECT * FROM sailors WHERE sname LIKE "A%Y";

#f. List information of all red boat
SELECT * FROM boat WHERE color="red";

#g. List information about boat reservation in October 1998
SELECT * FROM reserves WHERE DAY>="1998-10-1" AND DAY<="1998-10-31";

#h. Find the names of sailors who have reserved boat 103
SELECT sname FROM sailors, reserves WHERE bid=103 AND sailors.sid=reserves.sid;

#i. Find the names of sailors who have reserved a red boat
SELECT DISTINCT sname FROM sailors, reserves,boat
WHERE color="red" AND boat.bid=reserves.bid AND sailors.sid=reserves.sid;

#j. Find the names of sailors who have reserved at least one boat
SELECT DISTINCT sname FROM sailors NATURAL JOIN reserves;

#k. Find the names of sailors who have reserved a red  or a green boat
SELECT DISTINCT sname, sailors.sid FROM sailors, reserves, boat 
WHERE sailors.sid=reserves.sid 
AND reserves.bid=boat.bid
AND (color="red" OR color="green");

#l. Find the names of sailors who have reserved a red and a green boat
SELECT DISTINCT s.*
FROM sailors AS s NATURAL JOIN reserves AS r NATURAL JOIN boat AS b
WHERE b.color = 'red' AND s.sid IN
(SELECT r.sid
FROM reserves AS r NATURAL JOIN boat AS b
WHERE b.color = 'green');

#m. Find pairs of boats that have the same name
SELECT * FROM boat AS b1, boat AS b2
WHERE b1.bname=b2.bname AND b1.bid>b2.bid;