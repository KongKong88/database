
#1. List name and address of all clients
SELECT distinct NAME, clientaddress
FROM client;

#2. list information about client named Christ
SELECT DISTINCT *
FROM client
WHERE NAME LIKE "%Christ%";

#3. List information about client from Cow City
SELECT *
FROM client
WHERE clientaddress LIKE "%Cow City%";

#4. List information of rent that costs more than 800
SELECT *
FROM rent
WHERE price > 800;

#5. List information of rent that start after 2012
SELECT *
FROM rent
WHERE rent_date >= "2013-01-01";

#6. List ID of property rented by client C3
SELECT propertyID
FROM rent
WHERE clientID="C3";

#7. List type and address of properties rented by client C3
SELECT p.type, p.address
FROM rent AS r JOIN property AS p
	ON r.propertyID=p.propertyID
WHERE clientID="C3";

#8. List information of property rented by John
SELECT DISTINCT p.*
FROM rent AS r JOIN property AS p JOIN client AS c
	ON r.propertyID=p.propertyID AND c.clientID=r.clientID
WHERE c.name LIKE "John";

#9. List information of client who rent an apartment
SELECT DISTINCT c.*
FROM rent AS r JOIN property AS p JOIN client AS c
	ON r.propertyID=p.propertyID AND c.clientID=r.clientID
WHERE p.type LIKE "%apartment%";

#10. list information of client who has never rent a property
SELECT *
FROM client 
WHERE clientID NOT IN ( SELECT clientID
								FROM rent);
								
#11 List the property with the lowest rental price. 
SELECT property.*
FROM rent JOIN property
ON rent.propertyID=property.propertyID
WHERE price IN (SELECT MIN(price)
					FROM rent);
						
#12. For client who has rented the same property multiple times, list client’s id, client’s name,
#property id, rent_date and price order by client id and rent date.
SELECT distinct clientID, name, propertyID,result.rent_date,result.price
FROM (SELECT propertyID,clientID,rent_date,price,COUNT(clientID) AS T
    	FROM rent GROUP BY clientID,propertyID HAVING T>1) AS result
		  	NATURAL JOIN property NATURAL JOIN client
ORDER BY clientID, result.rent_date;

#13. Show the maximum, minimum and average rental price of each property.
SELECT MAX(price) AS maximum, MIN(price) AS minimum, AVG(price) AS average, propertyID
FROM rent 
GROUP BY propertyID;

#14. Show house in Cow City that is available for rent between January 2018 and December 2020

SELECT *
FROM rent AS r JOIN property AS p
 ON r.propertyID=p.propertyID
WHERE p.address LIKE "%cow city%" AND r.rent_date NOT IN
 ( SELECT rent_date
  FROM rent
  WHERE rent.rent_date>="2018-%-%" AND rent.end_date<="2020-%-%");
