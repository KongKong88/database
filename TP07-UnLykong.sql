USE movierating;

#1. For each rating that is the lowest (fewest stars) currently in the database
#return the reviewer name, movie title, and number of stars
SELECT m.mID, m.title, rw.name, r.stars
FROM movie AS m JOIN rating AS r JOIN reviewer AS rw
	ON m.mID=r.mID AND r.rID=rw.rID
WHERE stars IN (SELECT MIN( stars) FROM rating);

#2.List movie titles and average ratings, from highest-rated to lowest-rated
#If two or more movies have the same average rating, list them in alphabetical order
SELECT m.mID, m.title, AVG(stars)
FROM movie AS m JOIN rating AS r
	ON m.mID=r.mID
GROUP BY m.mID
ORDER BY AVG(r.stars) DESC, title;

#3. Find the names of all reviewers who have contributed three or more ratings
SELECT r.rID, rw.name, COUNT(r.rID)
FROM rating AS r JOIN reviewer AS rw
ON r.rID=rw.rID
GROUP BY r.rID
HAVING COUNT(r.rID)>=3;

#4. The same as 3 but try writing the query without having or without count
SELECT DISTINCT NAME
FROM rating r1, rating r2, rating r3 NATURAL JOIN reviewer
WHERE (r1.rID=r2.rID and r2.rID=r3.rID)
          AND(r1.mID,r1.stars) != (r2.mID,r2.stars)
          AND(r1.mID,r1.stars) != (r3.mID,r3.stars)
          AND(r2.mID,r2.stars) != (r3.mID,r3.stars);

#5. Some directors directed more than one movie. For all such directors, return the tiles of 
# all movies direccted by them, along with the director name. Sort by director name, then 
# movie title
SELECT m1.mID, m1.title, m1.director
FROM movie AS m1 , movie AS m2
WHERE m1.title!=m2.title AND m1.director=m2.director
ORDER BY m1.director, m1.title;

#6. Find the movie(s) with the highest average rating. Return the movie title(s) and average
#rating
CREATE VIEW movieavgstars AS 
	SELECT movie.* , AVG(stars) AS avgstars
	FROM movie NATURAL JOIN rating
	GROUP BY mID
	ORDER BY AVG(stars) DESC, title;

SELECT mid,title, avgstars
FROM movieavgstars
WHERE avgstars =(  SELECT MAX(avgstars)
						 FROM movieavgstars); 
								    
#7. Find the movie(s) with the lowest average rating. Return the movie title(s) and average 
#rating
SELECT mid,title, avgstars
FROM movieavgstars
WHERE avgstars = ( SELECT MIN(avgstars)
						 FROM movieavgstars);
						  
#8. For each director, return the director's name together with the title(s) of the movie(s)
#they directed that received the highest rating among all of their movies, and the value of
#that rating. Ignore movies whose director is NULL
CREATE VIEW max_stars AS
SELECT director, MAX(avgstars) AS highestStars
FROM movieavgstars
WHERE NOT ISNULL(director)
GROUP BY director; 

SELECT mid,max_stars.director, title
FROM max_stars JOIN movieavgstars
	ON max_stars.director=movieavgstars.director
WHERE max_stars.highestStars=movieavgstars.avgstars;

#9. For each movie that has at least one rating, find the highest number of stars that movie
#received. Return the movie title and number of stars. Sort by movie title
create view highest_stars AS
SELECT mid,MAX(stars) AS highest_stars 
FROM rating 
GROUP BY mid;

SELECT title,highest_stars
FROM movie NATURAL JOIN highest_stars
ORDER BY title;

#10. For each movie, return the title and the rating spread that is the difference between
#highest and lowest ratings given to that movie. Sort by rating spread from highest to lowest,
#then by movie title
SELECT title, ratingspread
FROM movie NATURAL JOIN ( SELECT MID, (MAX(stars)-MIN(stars)) AS ratingspread
								  FROM rating 
								  GROUP BY MID) AS ratingspread
ORDER BY ratingspread DESC, title;
