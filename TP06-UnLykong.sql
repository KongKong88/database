USE movierating;

#1. Find the titles of all movies directed by "Steven Spielberg"
SELECT title
FROM movie
WHERE director LIKE "%Steven Spielberg%";

#2. Find all years that have a movie that recieved a rating of 4 or 5,
#and sort them in increasing order
SELECT DISTINCT YEAR, stars, m.mID, title
FROM movie AS m JOIN rating AS r
	ON m.mID=r.mID
WHERE r.stars=4 OR r.stars=5
ORDER BY stars ASC;

#3. Find the titles of all movies that have no rating
SELECT title, mID
FROM movie
WHERE mID NOT IN ( SELECT mID FROM rating); 

#4. Some reviewers didn't provide a date with their rating. Find the names of all reviewers
#who have ratings with a NULL value for the date.
SELECT rw.name, r.ratingDate
FROM movie AS m JOIN reviewer AS rw JOIN rating AS r
	ON m.mID=r.mID AND rw.rID=r.rID
WHERE ISNULL(r.ratingDate);

#5. Write a query to return the ratings data in a more readable format: reviewer name, movie
#title, stars, and ratingDate. Also, sort the data, first by reviewer name, then by movie title,
#and lastly by number of stars.
SELECT rw.name, m.title, stars, r.ratingDate
FROM movie AS m JOIN rating AS r JOIN reviewer AS rw
	ON m.mID=r.mID AND rw.rID=r.rID
ORDER BY rw.name, title, stars;

#6. Find the name with reviewer who rated "Gone with the wind"
SELECT m.mID, rw.name , m.title
FROM movie AS m JOIN reviewer AS rw JOIN rating AS r
ON m.mID=r.mID AND rw.rID=r.rID
WHERE title="Gone with the wind";

#7. For any rating where the reviewer is the same as the director of the movie, return the
#reviewer name, movie title, and number of stars.
SELECT m.mID, rw.name, m.title, r.stars
FROM movie AS m JOIN reviewer AS rw JOIN rating AS r
	ON m.mID=r.mID AND rw.rID=r.rID
WHERE director=NAME;

#8. Return all reviewer names and movie names together in a single list, alphabetized.
SELECT rw.name, m.title
FROM movie AS m JOIN rating AS r JOIN reviewer AS rw
	ON m.mID=r.mID AND r.rID=rw.rID
ORDER BY NAME;

#9. Find the titles of all movies not reviewed by “Chris Jackson”.
SELECT DISTINCT MID, title
FROM movie
WHERE title NOT IN ( SELECT title 
							FROM movie AS m JOIN rating AS r JOIN reviewer AS rw
							ON m.mID=r.mID AND rw.rID=r.rID
							WHERE rw.name="Chris Jackson");
							
#10.For all pairs of reviewers such that both reviewers gave a rating to the same movie, 
# return the names of both reviewers. Eliminate duplicates, don't pair reviewers 
# with themselves, and include each pair only once. For each pair, return the names 
# in the pair in alphabetical order.
SELECT DISTINCT rw1.name, rw2.name
FROM movie AS m JOIN rating AS r1 JOIN rating AS r2 JOIN reviewer AS rw1 JOIN reviewer AS rw2
	ON m.mID=r1.mID AND m.mID=r2.mID AND rw1.rID=r1.rID AND rw2.rID=r2.rID
WHERE rw1.name<rw2.name 
ORDER BY rw1.name<rw2.name, rw1.name;



