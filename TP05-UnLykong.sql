USE producttest;
#1. Find all products ordered by "John Doe"
SELECT DISTINCT p.* 
FROM customer_t AS c JOIN order_line_t AS ol JOIN order_t AS o JOIN product_t AS p
	ON c.ID=o.customer_ID AND p.ID=ol.product_ID AND ol.order_ID=o.ID
WHERE c.name="John Doe";

#2. Find customer who has ordered any products ordered by "John Doe"
SELECT DISTINCT c.*
FROM customer_t AS c JOIN order_line_t AS ol JOIN order_t AS o JOIN product_t AS p
	ON c.ID=o.customer_ID AND p.ID=ol.product_ID AND ol.order_ID=o.ID
WHERE c.name!="John Doe" AND p.ID IN 
	(SELECT p.ID
	FROM customer_t AS c JOIN order_line_t AS ol JOIN order_t AS o JOIN product_t AS p
		ON c.ID=o.customer_ID AND p.ID=ol.product_ID AND ol.order_ID=o.ID
	WHERE c.name="John Doe");
	
#3. Find customer who has ordered any products finished in "Natural Ash"
SELECT DISTINCT c.* 
FROM customer_t AS c JOIN order_line_t AS ol JOIN order_t AS o JOIN product_t AS p
	ON c.ID=o.customer_ID AND p.ID=ol.product_ID AND ol.order_ID=o.ID
WHERE p.finish LIKE "%Natural Ash%";

#4. Find order that contains more than 2 products
SELECT *
FROM order_t AS o
WHERE o.ID IN ( SELECT ol.order_ID
					FROM order_line_t AS ol
					GROUP BY ol.order_ID
					HAVING COUNT(ol.product_ID)>2 );
	
#5. Find customer who has make more than one order
SELECT *
FROM customer_t AS c
WHERE c.ID IN ( SELECT o.customer_ID
					FROM order_t AS o
					GROUP BY o.customer_ID
					HAVING COUNT(o.ID>1) );
	
#6. Pair customer and his/her total number of orders
SELECT DISTINCT c.*, SUM(quantity)
FROM customer_t AS c JOIN product_t AS p JOIN order_t AS o JOIN order_line_t AS ol
ON c.ID=o.customer_ID AND o.ID=ol.order_ID AND p.ID=ol.product_ID
GROUP BY c.ID;

#7. Find the total number of unit of "office chair" sold in April 2004
SELECT SUM(quantity)
FROM product_t AS p JOIN order_t AS o JOIN order_line_t AS ol
	ON p.ID=ol.product_ID AND o.ID=ol.order_ID
WHERE p.description="office chair" AND
	# (  o.order_date>="2004-04-01" AND o.order_date<="2004-04-30");
	(o.order_date BETWEEN "2004-04-01" AND "2004-04-30");
	
#8. Find the total cost of order made by "John Doe"
SELECT SUM(standard_price*quantity)
FROM product_t AS p JOIN order_t AS o JOIN order_line_t AS ol JOIN customer_t AS c
	ON c.ID=o.customer_ID AND o.ID=ol.order_ID AND p.ID=ol.product_ID
WHERE c.name="John Doe";

#9. Find the average cost of order made by "John Doe"
SELECT AVG(standard_price*quantity) AS total_cost
FROM product_t AS p JOIN order_t AS o JOIN order_line_t AS ol JOIN customer_t AS c
	ON c.ID=o.customer_ID AND o.ID=ol.order_ID AND p.ID=ol.product_ID
WHERE c.name="John Doe";

#10. Find the order that contain all products made from cherry
SELECT *
FROM order_t as o
WHERE NOT EXISTS( SELECT *
                	FROM product_t
                	WHERE finish LIKE '%cherry%' AND 
                    id NOT IN ( SELECT product_id
                                	FROM order_line_t
                                	WHERE order_id = o.id));
                                	
#11. Find customer who has order all kind of table lamp
SELECT  *
FROM  customer_t AS c
WHERE NOT EXISTS  (SELECT ID
						FROM product_t
						WHERE  description LIKE "%table lamp%" AND ID NOT IN 
								(SELECT ol.product_ID	
								FROM order_t AS o JOIN order_line_t AS ol
								ON o.id = ol.order_ID 
								WHERE o.customer_ID = c.ID) 
						);