USE classicmodels;

#1. Show first name and last name of customer from France
SELECT DISTINCT  customerNumber, contactLastName, contactFirstName
FROM customers 
WHERE country="France";

#2. Show the first name and last name of employee who is a sale manager
SELECT DISTINCT employeeNumber, lastname, firstname
FROM employees
WHERE jobTitle LIKE "sale% manager%";

#3. Show all product which is in "classic car" product line
SELECT DISTINCT productCode, productName, productLine, productScale, productVendor, productDescription
FROM products
WHERE products.productLine LIKE "%classic car%";

#4. Show all products containing "Toyota" in its name
SELECT DISTINCT productCode, productName, productLine, productScale, productVendor, productDescription
FROM products
WHERE products.productName LIKE "%toyota%";

#5. Show all products with the scale smaller than 1:24
SELECT DISTINCT productCode, productName, productLine, productScale, productVendor, productDescription
FROM products
WHERE SUBSTR(productScale, 3)>24;

#6. Show all products from the vendor "Autoart Studio Design"
SELECT DISTINCT productCode, productName, productLine, productScale, productVendor, productDescription
FROM products
WHERE products.productVendor="Autoart Studio Design"; 

#7. Show all products with MSRP(the manufacturer's suggested retail priice)
#higher than twice the cost of buyPrice 
SELECT DISTINCT productCode, productName, productLine, productScale, productVendor, productDescription
FROM products
WHERE MSRP>2*buyPrice;

#8. Show information of sale representative of the customer "Land of toys Inc"
SELECT customerNumber, salesRepEmployeeNumber
FROM customers
WHERE customers.customerName LIKE "%land of toys Inc%";

#9. Show information about order that is shipped later than the required date
SELECT DISTINCT *
FROM orders
WHERE shippedDate>requiredDate;

#10. Show all orders made by "La Rochelle Gifts"
SELECT DISTINCT o.*
FROM customers AS c JOIN orders AS o
	ON c.customerNumber=o.customerNumber
WHERE c.customerName LIKE "%la rochelle gifts%";

#11. Show all products ordered by "La rochelle gifts"
SELECT DISTINCT p.*
FROM customers AS c JOIN orders AS o JOIN orderdetails AS od JOIN products AS p
	ON c.customerNumber=o.customerNumber AND o.orderNumber=od.orderNumber AND 
		p.productCode=od.productCode
WHERE c.customerName LIKE "%la rochelle gifts%";

#12. Show customer's name who has ordered more than 15 products in once
SELECT c.customerName, COUNT(od.productCode)AS countOrders
FROM customers AS c JOIN orders AS o JOIN orderdetails AS od
	ON c.customerNumber=o.customerNumber AND o.orderNumber=od.orderNumber
GROUP BY o.orderNumber
HAVING COUNT(od.productCode)>15;

#13. Show the best-selling product in "2004-02"												 
CREATE VIEW orderinformations AS 
SELECT *
FROM orderdetails AS od NATURAL JOIN orders AS o
WHERE o.orderDate BETWEEN "2004-02-01" AND "2004-02-29";

CREATE VIEW sumProduct AS 
SELECT oi.orderNumber, oi.productCode, SUM(oi.quantityOrdered) AS sumQuantity,
		 oi.orderDate, p.productName
FROM orderinformations AS oi JOIN products AS p
	ON oi.productCode=p.productCode
GROUP BY oi.productCode;

SELECT oi.orderNumber, oi.productCode, oi.orderDate, sp.productName, sp.sumQuantity
FROM orderinformations AS oi NATURAL JOIN sumproduct AS sp
WHERE sumQuantity IN (SELECT MAX(sumQuantity)
											 FROM sumproduct);

												 









